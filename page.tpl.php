<?php

switch ($layout) {
  case 'both':
    $mollio_layout = 'type-c';
    break;
  case 'left':
    $mollio_layout = 'type-b';
    break;
  case 'right':
    $mollio_layout = 'type-d';
    break;
  case 'none':
  default:
    $mollio_layout = 'type-a';
    break;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language; ?>" xml:lang="<?php print $language; ?>">
<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?>
<?php print $styles; ?>
<!--[if lte IE 6]>
<link rel="stylesheet" type="text/css" href="<?php print base_path() . $directory; ?>/ie6_or_less.css" />
<![endif]-->
<script type="text/javascript" src="<?php print base_path() . $directory; ?>/common.js"></script>
</head>
<body id="<?php print $mollio_layout; ?>">
<div id="wrap">
  <div id="header">
    <?php if ($site_name): ?>
    <div id="site-name">
      <?php print $site_name; ?>
      <?php if ($site_slogan): ?>
      <span class="site-slogan"><?php print $site_slogan; ?></span>
      <?php endif; ?>
    </div>
    <?php endif; ?>

    <?php if ($search_box): ?>
    <div id="search">
      <form id="search_theme_form" class="search" action="" method="post">
        <input type="hidden" name="edit[form_id]" id="edit-form_id" value="search_theme_form"  />
        <div class="container-inline">
          <label for="edit-search_theme_form_keys"><?php print t('Search:'); ?></label>
          <input class="form-text" type="text" size="15" value="" name="edit[search_theme_form_keys]" id="edit-search_theme_form_keys" />
          <input class="form-submit" type="submit" name="op" value="<?php print t('Go'); ?>" /><br />
        </div>
      </form>
    </div>
    <?php endif; ?>

    <?php print $header; ?>
  </div>

  <div id="content-wrap">
    <?php if ($sidebar_left): ?>
    <div id="utility"><?php print $sidebar_left; ?></div>
    <?php endif; ?>

    <div id="content">
      <div id="breadcrumb"><?php print $breadcrumb . $title; ?></div>

      <?php if ($is_front && $mission): ?>
      <div class="featurebox"><?php print $mission; ?></div>
      <?php endif; ?>

      <h1><?php print ($title ? $title : $site_name); ?></h1>

      <?php if ($tabs): print $tabs; endif; ?>

      <?php if ($help): ?>
      <div class="help"><?php print $help; ?></div>
      <?php endif; ?>

      <?php if ($messages): ?>
      <div class="messages"><?php print $messages; ?></div>
      <?php endif; ?>

      <?php print $content; ?>

      <div id="footer"><?php print $footer_message; ?></div>
    </div>

    <?php if ($sidebar_right): ?>
    <div id="sidebar"><?php print $sidebar_right; ?></div>
    <?php endif; ?>

    <?php if ($logo): ?>
    <div id="poweredby"><a href="<?php print base_path(); ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="" /></a></div>
    <?php endif; ?>
  </div>
</div>

<?php print $closure; ?>
</body>
</html>
