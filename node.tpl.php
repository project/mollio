<?php /* $Id$ */ ?>
<div id="node-<?php print $node->nid; ?>" class="node<?php print (!$page && $sticky) ? " featurebox" : ""; ?>">
  <?php if (!$page && $title): ?>
  <h2>
    <?php if ($submitted): ?>
    <span class="date"><?php print $submitted; ?></span>
    <?php endif; ?>
    <a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a>
  </h2>
  <?php endif; ?>
  <?php print $picture; ?>
  <?php print $content; ?>
  <?php print ($picture ? '<br class="clear" />' : ''); ?>
  <?php if ($terms): ?>
  <div class="terms"><?php print t('posted in:'); ?> <?php print implode(', ', $taxonomy); ?></div>
  <?php endif; ?>
  <div class="links"><?php print $links; ?></div>
</div>

<hr />
