<?php

// custom navigation links
function mollio_nav($parent, $items = array(), $main_class = '') {
  $i = 0;
  $class = '';
  $wrapper_prefix = '<li>';
  $wrapper_suffix = '</li>';
  $output = '';

  if (count($items) > 0) {
    $output .= "<ul>";
    foreach ($items as $item) {
      $class = '';
      if ($i == 0) {
        $class = 'first';
      }
      elseif ($i == count($items) - 1) {
        $class = 'last';
      }

      if (strstr($item, 'class="active"') !== FALSE) {
        $class .= ($class ? ' ' : '') . 'active';
        $main_class .= ($main_class != '' ? ' ' : '') . 'active';
        $parent = str_replace('<a href=', '<a class="active" href=', $parent);
      }

      $class = ' class="' . $class . '"';
      $output .= '<li' . $class . '>' . $item . '</li>';
      $i++;
    }
    $output .= "</ul>";
  }
  else {
    if (strstr($parent, 'class="active"') !== FALSE) {
      $main_class .= ($main_class != '' ? ' ' : '') . 'active';
    }
  }

  $wrapper_prefix = '<li class="' . $main_class . '">';
  $output = $wrapper_prefix . $parent . $output . $wrapper_postfix;
  return $output;
}

// custom implementation of theme_list
function mollio_list($items, $type = 'ul') {
  $i = 0;
  $class = '';
  $output = '';
  $type = strtolower($type);
  if ($type != 'ol' && $type != 'dl') {
    $type = 'ul';
  }
  $output .= "<$type>";
  foreach ($items as $item) {
    $class = '';
    if ($i == 0) {
      $class = 'first';
    }
    elseif ($i == count($items) - 1) {
      $class = 'last';
    }

    if (strstr($item, 'class="active"') !== FALSE) {
      $class .= ($class?' ':'') . 'active';
    }

    $class = ' class="' . $class . '"';
    $output .= '<li' . $class . '>' . $item . '</li>';
    $i++;
  }
  $output .= "</$type>";
  return $output;
}

// custom breadcrumbs
function mollio_breadcrumb($breadcrumb) {
  $sep = ' / ';
  if (count($breadcrumb) > 0) {
    return implode($breadcrumb, $sep) . $sep;
  }
  else {
    return t("Home");
  }
}

// custom menu output
// menu_in_active_trail($mid)
function mollio_menu_tree($pid = 1) {
  if ($tree = menu_tree($pid)) {
    if ($pid == variable_get('menu_primary_menu', 1)) {
      return "<ul class=\"menu\">\n" . $tree . "</ul>\n";
    }
    elseif ($pid == variable_get('menu_secondary_menu', 2)) {
      return "<ul id=\"nav\">\n" . $tree . "</ul>\n";
    }
    else {
      return "<ul>\n" . $tree . "</ul>\n";
    }
  }
}

function mollio_menu_item($mid, $children = '', $leaf = TRUE) {
  $class = '';
  $elipses = '';

  if (strstr($children, 'class="active"') !== FALSE || strstr(menu_item_link($mid), 'class="active"') !== FALSE) {
    $class = ' class="active"';
  }

  if (!$leaf) {
    $elipses = '...';
  }

  return '<li' . $class. '>'. str_replace('</a>', "$elipses</a>", menu_item_link($mid)) . $children . "</li>\n";
}

function mollio_menu_item_link($item, $link_item) {
  $attrs = array();
  if (isset($item['description'])) {
    $attrs = array('title' => $item['description']);
  }
  return l($item['title'], $link_item['path'], $attrs);
}

// custom pager output
function mollio_pager($tags = array(), $limit = 10, $element = 0, $parameters = array()) {
  global $pager_total;
  $output = '';

  if ($pager_total[$element] > 1) {
    $output .= '<div class="pagination">';
    $output .= '<p>';
    //$output .= theme('pager_first', ($tags[0] ? $tags[0] : t('First')), $limit, $element, $parameters);
    $output .= theme('pager_previous', ($tags[1] ? $tags[1] : t('Previous')), $limit, $element, 1, $parameters);
    $output .= theme('pager_list', $limit, $element, ($tags[2] ? $tags[2] : 9 ), '', $parameters);
    $output .= theme('pager_next', ($tags[3] ? $tags[3] : t('Next')), $limit, $element, 1, $parameters);
    $output .= theme('pager_last', ($tags[4] ? $tags[4] : t('Last')), $limit, $element, $parameters);
    $output .= '</p>';

    $output .= '<h4>';
    $current = intval($pager_total[$element]);
    //$current_page = $element . ', ' . $current . '/' . $limit;
    $current_page = floor($current / $limit) + 1;
    $total_pages = ceil($pager_total[$element] / $limit) + 1;
    //$output .= sprintf('%d of %d', $current_page, $total_pages);
    $output .= $current_page . ' of ' . $total_pages;
    $output .= '</h4>';
    $output .= '</div>';

    return $output;
  }
}

function mollio_pager_first($text, $limit, $element = 0, $parameters = array()) {
  global $pager_page_array;
  $output = '';
  if ($pager_page_array[$element] > 0) {
    $output = theme('pager_link', $text, pager_load_array(0, $element, $pager_page_array), $element, $parameters, array('class' => 'pager-first'));
  }
  return $output;
}

function mollio_pager_last($text, $limit, $element = 0, $parameters = array()) {
  global $pager_page_array, $pager_total;
  $output = '';
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $output = theme('pager_link', $text, pager_load_array($pager_total[$element] - 1, $element, $pager_page_array), $element, $parameters, array('class' => 'pager-last'));
  }
  return $output;
}

function mollio_pager_list($limit, $element = 0, $quantity = 5, $text = '', $parameters = array()) {
  global $pager_page_array, $pager_total;

  $output = '<span class="pager-list">';

  // Calculate various markers within this pager piece:
  $pager_middle = ceil($quantity / 2);
  $pager_current = $pager_page_array[$element] + 1;
  $pager_first = $pager_current - $pager_middle + 1;
  $pager_last = $pager_current + $quantity - $pager_middle;
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  // When there is more than one page, create the pager list.
  if ($i != $pager_max) {
    $output .= $text;
    if ($i > 1) {
      $output .= '<span class="pager-ellipsis">…</span>';
    }

    // Now generate the actual pager piece.
    for (; $i <= $pager_last && $i <= $pager_max; $i++) {
      if ($i < $pager_current) {
        $output .= theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters);
      }
      if ($i == $pager_current) {
        $output .= '<strong class="pager-current">'. $i .'</strong>';
      }
      if ($i > $pager_current) {
        $output .= theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters);
      }
    }

    if ($i < $pager_max) {
      $output .= '<span class="pager-ellipsis">…</span>';
    }
  }
  $output .= '</span>';

  return $output;
}

function mollio_pager_next($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', $text, $limit, $element, $parameters);
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', $text, $page_new, $element, $parameters, array('class' => 'pager-next'));
    }
  }

  return $output;
}

function mollio_pager_previous($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('pager_first', $text, $limit, $element, $parameters);
    }
    // The previous page is not the first page.
    else {
      $output = theme('pager_link', $text, $page_new, $element, $parameters, array('class' => 'pager-previous'));
    }
  }

  return $output;
}

function mollio_pager_link($text, $page_new, $element, $parameters = array(), $attributes = array()) {
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query[] = drupal_query_string_encode($parameters, array());
  }
  $querystring = pager_get_querystring();
  if ($querystring != '') {
    $query[] = $querystring;
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = null;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    else if (is_numeric($text)) {
      $attributes['title'] = t('Go to page %number', array('%number' => $text));
    }
  }

  return '<strong>' . l($text, $_GET['q'], $attributes, count($query) ? implode('&', $query) : NULL) . '</strong>';
}

// custom form elements
function mollio_form_element($title, $value, $description = NULL, $id = NULL, $required = FALSE, $error = FALSE) {
  $output = '';
  $required = $required ? '<span class="req">*</span>' : '';

  if ($title) {
    if ($id) {
      $output .= "<label for=\"$id\"><b>$required$title:</b>";
    }
    else {
      $output .= "<label><b>$required$title:</b>";
    }
  }

  $output .= $value . "<br />\n";

  if ($title) {
    $output .= '</label>' . "\n";
  }

  if ($description) {
    $output .= "<div class=\"description\">$description</div>\n";
  }

  return $output;
}

// custom search results
function mollio_search_item($item, $type) {
  $output = '';

  if (module_hook($type, 'search_item')) {
    $output = module_invoke($type, 'search_item', $item);
  }
  else {
    $output .= '<dd>' . "\n";
    $output .= '  <dl>' . "\n";
    $output .= '    <dt class="title"><a href="'. check_url($item['link']) .'">'. check_plain($item['title']) .'</a></dt>' . "\n";
    $output .= '    <dd class="desc">' . $item['snippet'] . '</dd>' . "\n";
    if ($item['type']) {
      $output .= '    <dd class="filetype">' . $item['type'] . '</dd>' . "\n";
    }
    if ($item['date']) {
      $output .= '    <dd class="date">' . format_date($item['date'], 'small') . '</dd>' . "\n";
    }

    $info = array();
    if ($item['user']) {
      $info[] = $item['user'];
    }
    if (is_array($item['extra'])) {
      $info = array_merge($info, $item['extra']);
    }
    $output .= '    <dd>'. implode(' - ', $info) .'</dd>' . "\n";
    $output .= '  </dl>' . "\n";
    $output .= '</dd>' . "\n";
  }

  return $output;
}

?>
