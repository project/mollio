<?php /* $Id$ */ ?>
<div id="comment-<?php print $comment->cid; ?>" class="comment<?php print ($comment->new)?" comment-new":""; ?>">
  <?php if ($title): ?>
  <h3>
    <?php if ($submitted): ?>
    <span class="date"><?php print $submitted; ?></span>
    <?php endif; ?>
    <?php print $title . ($comment->new ? " <a id=\"new\"></a><span class=\"highlight\">$new</span>" : ''); ?>
  </h3>
  <?php endif; ?>
  <?php print $picture; ?>
  <?php print $content; ?>
  <?php print ($picture ? '<br class="clear" />' : ''); ?>
  <div class="links"><?php print  $links; ?></div>
</div>

<hr />
