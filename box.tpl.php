<?php /* $Id$ */ ?>
<div class="box">
  <?php if ($title): ?>
  <h2><?php print $title; ?></h2>
  <?php endif; ?>
  <?php print $content; ?>
</div>
