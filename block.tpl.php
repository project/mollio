<?php /* $Id$ */ ?>
<?php if ($block->region != 'header' && $block->module != 'menu'): ?>
<div id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>" class="block<?php print " block-$block->module"; ?><?php print ($block->region == 1 || $block->region == 'right') ? ' featurebox': ''; ?>">
  <?php if ($block->subject): ?>
  <h4><?php print $block->subject; ?></h4>
  <?php endif; ?>
  <?php print $block->content; ?>
</div>
<?php else: ?>
  <?php print $block->content; ?>
<?php endif; ?>
