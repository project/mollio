
Based on Mollio CSS/HTML Templates
Copyright: Daemon Pty Limited 2006, http://www.daemon.com.au
Community: http://www.mollio.org/

Drupal PHPTemplate implementation
Copyright (C) 2006 Standard Interactive
Relased under the GNU General Public License, version 2.0

Author: Rowan Kerr <rkerr@sri.ca>

Standard Interactive
2 St Clair Ave W
Toronto, ON
Canada
M4V 1L6

http://www.standardinteractive.ca/

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

========================================================================

How to configure the top navigation:
- Put the primary links block into the header region
- Set all sub-menus to expanded


